<?php
/**
 * The template for displaying all single posts.
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'elysio_container_type' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

					<?php elysio_post_nav(); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->


		
		 <?php
		 if( get_theme_mod( 'blog_single_singlenav_checkbox' ) != 1){

			$single_setting_recent_count = get_theme_mod( 'single_setting_recent_count', '3' );
			if( !$single_setting_recent_count ) {
				$single_setting_recent_count = 3;
			}

			$categories = get_the_category();

			$rp_query = new WP_Query(array(
				'posts_per_page'        => $single_setting_recent_count,
				'post__not_in'          => array( $post->ID ),
				'cat'                   => $categories[0]->term_id,
				'orderby'        => 'rand',
			));

			if( $rp_query->have_posts() ){
				echo '<div class="elysio-related-posts">';
					echo '<h2 class="elysio-related-posts__title">Related Articles</h2>';
					echo '<div class="row">';
					while( $rp_query->have_posts() ){
						$rp_query->the_post();
						get_template_part( 'loop-templates/related-posts' );
					}
					echo '</div>';
				echo '</div>';
			}
		}
		 ?>

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php get_footer(); ?>
