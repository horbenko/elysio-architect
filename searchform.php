<?php
/**
 * The template for displaying search forms
 *
 * @package elysio-architect
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<form method="get" class="searchform material-form" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">


	<div class="group">
	    <input id="s" name="s" type="text" required="" value="<?php the_search_query(); ?>"><span class="highlight"></span><span class="bar"></span>
	    <label><?php esc_html_e( 'Search', 'elysio-architect' ); ?></label>
  	</div>

</form>


