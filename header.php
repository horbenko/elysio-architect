<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'elysio_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	
</head>

<body <?php body_class('custom-background custom-color'); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	
	<?php

	// Header Layout Settings
	$elysio_header_menu_position = get_theme_mod( 'elysio_header_menu_position' );


	$humburger_menu_classes = '';
	if( $elysio_header_menu_position == 'left' ){
		$humburger_menu_classes .= ' mr-auto';
	}
	if( $elysio_header_menu_position == 'right' ){
		$humburger_menu_classes .= ' ml-auto';
	}

	$elysio_header_menu_position_classes = '';

	if( $elysio_header_menu_position == 'left'){
		$elysio_header_menu_position_classes = 'justify-content-start';
	}
	if( $elysio_header_menu_position == 'center'){
		$elysio_header_menu_position_classes = 'justify-content-center';
	}
	if( $elysio_header_menu_position == 'justify'){
		$elysio_header_menu_position_classes = 'justify-content-around';
	}
	if( $elysio_header_menu_position == 'right'){
		$elysio_header_menu_position_classes = 'justify-content-end';
	}



	// if( get_theme_mod( 'header_fixed_checkbox' ) ){
	// 	$elysio_header_fixed = 'fixed-top';
	// }else{
	// 	$elysio_header_fixed = '';
	// }

	$elysio_header_navbar_classes = '';

	if( get_theme_mod( 'header_shadow' ) ){
		$elysio_header_navbar_classes .= ' header-shadow';
	}
	if( get_theme_mod( 'header_rounded' ) ){
		$elysio_header_navbar_classes .= ' header-rounded';
	}


	?>

	<header id="site-header" class="site-header" role="banner">

	  	<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'elysio-architect' ); ?></a>
	  	<nav class="navbar navbar-expand-md elysio-navbar <?php echo esc_attr( $elysio_header_navbar_classes ); ?>">

			<?php if ( 'container' == $container ) : ?>
				<div class="container">
			<?php endif; ?>


					<!-- Logo -->
					<?php if ( ! has_custom_logo() ) { ?>
						<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>
					<?php } else {
					the_custom_logo();
					} ?>


					<!-- Humburger Menu -->
					<button class="navbar-toggler <?php echo esc_attr( $humburger_menu_classes ); ?>" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'elysio-architect' ); ?>">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/></svg>
					</button>


					<!-- The WordPress Menu goes here -->
					<?php 
					// wp_nav_menu(
					//   array(
					//     'theme_location'  => 'primary',
					//     'container_class' => 'collapse navbar-collapse',
					//     'container_id'    => 'navbarNavDropdown',
					//     'menu_class'      => 'navbar-nav ' . $elysio_header_menu_position,
					//     'fallback_cb'     => '',
					//     'menu_id'         => 'main-menu',
					//     'depth'           => 2,
					//     'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					//   )
					// );
					?>
					<?php
					wp_nav_menu( array(
					'theme_location'	=> 'primary',
					'container_class'	=> 'main-navigation collapse navbar-collapse',
					'container_id'		=> 'navbarNavDropdown',
					'menu_class'		=> 'navbar-nav ' . $elysio_header_menu_position_classes,
					'fallback_cb'		=> '',
					'menu_id'			=> 'main-menu',
					'depth'				=> 3,
					) );
					?>

		  
					<!-- Header Search -->
					<?php elysio_header_searh_button(); ?>


			<?php if ( 'container' == $container ) : ?>
				</div><!-- .container -->
			<?php endif; ?>

		</nav>

	</header>