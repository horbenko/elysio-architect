<?php

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}


function ocdi_import_files() {
  return array(
    array(
      'import_file_name'           => 'Architect 1',
      'categories'                 => array( 'Category 1', 'Category 2' ),
      'import_file_url'            => 'http://www.your_domain.com/ocdi/demo-content.xml',
      'import_widget_file_url'     => 'http://www.your_domain.com/ocdi/widgets.json',
      'import_customizer_file_url' => 'http://www.your_domain.com/ocdi/customizer.dat',
      'import_redux'               => array(
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux.json',
          'option_name' => 'redux_option_name',
        ),
      ),
      'import_preview_image_url'   => 'http://localhost:8888/architec/wp-content/uploads/2019/04/home1-hero.jpg',
      'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'elysio-architect' ),
      'preview_url'                => 'http://www.your_domain.com/my-demo-1',
    ),
    array(
      'import_file_name'           => 'Architect 2',
      'categories'                 => array( 'New category', 'Old category' ),
      'import_file_url'            => 'http://www.your_domain.com/ocdi/demo-content2.xml',
      'import_widget_file_url'     => 'http://www.your_domain.com/ocdi/widgets2.json',
      'import_customizer_file_url' => 'http://www.your_domain.com/ocdi/customizer2.dat',
      'import_redux'               => array(
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux.json',
          'option_name' => 'redux_option_name',
        ),
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux2.json',
          'option_name' => 'redux_option_name_2',
        ),
      ),
      'import_preview_image_url'   => 'http://localhost:8888/architec/wp-content/uploads/2019/05/demo-2@2x.jpg',
      'import_notice'              => __( 'A special note for this import.', 'elysio-architect' ),
      'preview_url'                => 'http://www.your_domain.com/my-demo-2',
    ),
    array(
      'import_file_name'           => 'Architect 3',
      'categories'                 => array( 'New category', 'Old category' ),
      'import_file_url'            => 'http://www.your_domain.com/ocdi/demo-content2.xml',
      'import_widget_file_url'     => 'http://www.your_domain.com/ocdi/widgets2.json',
      'import_customizer_file_url' => 'http://www.your_domain.com/ocdi/customizer2.dat',
      'import_redux'               => array(
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux.json',
          'option_name' => 'redux_option_name',
        ),
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux2.json',
          'option_name' => 'redux_option_name_2',
        ),
      ),
      'import_preview_image_url'   => 'http://localhost:8888/architec/wp-content/uploads/2019/05/demo-2@2x.jpg',
      'import_notice'              => __( 'A special note for this import.', 'elysio-architect' ),
      'preview_url'                => 'http://www.your_domain.com/my-demo-2',
    ),
    array(
      'import_file_name'           => 'Architect Dark',
      'categories'                 => array( 'New category', 'Old category' ),
      'import_file_url'            => 'http://www.your_domain.com/ocdi/demo-content2.xml',
      'import_widget_file_url'     => 'http://www.your_domain.com/ocdi/widgets2.json',
      'import_customizer_file_url' => 'http://www.your_domain.com/ocdi/customizer2.dat',
      'import_redux'               => array(
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux.json',
          'option_name' => 'redux_option_name',
        ),
        array(
          'file_url'    => 'http://www.your_domain.com/ocdi/redux2.json',
          'option_name' => 'redux_option_name_2',
        ),
      ),
      'import_preview_image_url'   => 'http://localhost:8888/architec/wp-content/uploads/2019/05/demo-2@2x.jpg',
      'import_notice'              => __( 'A special note for this import.', 'elysio-architect' ),
      'preview_url'                => 'http://www.your_domain.com/my-demo-2',
    ),
  );
}