<?php

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}


function ah_register_required_plugins(){
  $plugins = [
    [
      'name'  => 'Elementor',
      'slug'  => 'elementor',
      'required'  => true
    ],
    [
      'name'  => 'Contact Form 7',
      'slug'  => 'contact-form-7',
      'required'  => false
    ],
    // [
    //   'name'  => 'One Click Demo Import',
    //   'slug'  => 'one-click-demo-import',
    //   'required'  => true
    // ],
    // [
    //   'name'  => 'Developer Share Buttons',
    //   'slug'  => 'developer-share-buttons',
    //   'required'  => false
    // ],
    // [
    //   'name'  => 'Elysio Toolkit',
    //   'slug'  => 'elysio-toolkit',
    //   'src'   =>  get_template_directory() . '/plugins/elysio-toolkit.zip',
    //   'required'  => true
    // ]
    array(
          'name'               => 'Elysio Toolkit', // The plugin name.
          'slug'               => 'elysio-toolkit', // The plugin slug (typically the folder name).
          'source'             => get_stylesheet_directory() . '/plugins/elysio-toolkit.zip', // The plugin source.
          'required'           => true, // If false, the plugin is only 'recommended' instead of required.
          'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
          'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
          'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
          'external_url'       => '', // If set, overrides default API URL and points to an external URL.
          'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
        ),
  ];

  $config = [
    'id'          => 'elysio-architec',
    'menu'        => 'tgmpa-install-plugins',
    'parent_slug' => 'themes.php',
    'capability'  => 'edit_theme_options',
    'has_notices' => true,
    'dismissable' => true
  ];

  tgmpa( $plugins, $config );
}