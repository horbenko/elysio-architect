<?php
/**
 * Custom template tags for Elysio theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/*  Header Search Button */

if ( ! function_exists( 'elysio_header_searh_button' ) ) {
	function elysio_header_searh_button() {

		if( get_theme_mod( 'header_show_search' ) || get_theme_mod( 'header_show_search_mobile' ) ){
			?>

			<div class="header_search-icon" id="hssm"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path d="M0 0h24v24H0z" fill="none"/></svg></div>

				<div class="header_search custom-background" id="headerSearch">
					<div class="header_search_modal-content">
						<div class="row">
							<div class="col-12">
								<div id="hscm" class="header_search-close_modal"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></div>
								<?php get_search_form(); ?>

								<!-- <input type="text" name="keyword" id="keyword" onkeyup="fetch()"></input> -->

								<!-- <div id="datafetch">Search results will appear here</div> -->

								<div id="headerSearchResult" class="header_search-result"></div>
							</div>
						</div>
					</div>
				</div>
			<?php
		}

	}
}





/*  Entry Author  */

if ( ! function_exists( 'elysio_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function elysio_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'Author: %s', 'post author', 'elysio-architect' ),
			'<span class="entry_author"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;




/*  Entry Categories  */

if ( ! function_exists( 'elysio_entry_cats' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function elysio_entry_cats() {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'elysio-architect' ) );
		if ( $categories_list ) {
			/* translators: 1: list of categories. */
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'elysio-architect' ) . '</span> ', $categories_list ); // WPCS: XSS OK.
		}
	}
endif;




/*  Entry Tags  */

if ( ! function_exists( 'elysio_entry_tags' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function elysio_entry_tags() {
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'elysio-architect' ) );
		if ( $tags_list ) {
			/* translators: 1: list of tags. */
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'elysio-architect' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}
endif;



/*  Entry Limpsum  */
// if ( ! function_exists( 'elysio_entry_limpsum' ) ) :
//   function elysio_entry_limpsum() {
		
//   }
// endif;






/**
 * Display navigation to next/previous post when applicable.
 */

if ( ! function_exists ( 'elysio_post_nav' ) ) {
	function elysio_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="container navigation post-navigation elysio-post-navigation">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'elysio-architect' ); ?></h2>
			<div class="row nav-links justify-content-between">
				<?php
				if ( get_previous_post_link() ) {
					previous_post_link( '<div class="col-12 col-sm-6 nav-previous">%link</div>', _x( '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"></path><path fill="none" d="M0 0h24v24H0V0z"></path></svg>&nbsp;%title', 'Previous post link', 'elysio-architect' ) );
				}
				if ( get_next_post_link() ) {
					next_post_link( '<div class="col-12 col-sm-6 nav-next">%link</div>', _x( '%title&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"></path><path fill="none" d="M0 0h24v24H0V0z"></path></svg>', 'Next post link', 'elysio-architect' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}




/* Contacts Bar */

if ( ! function_exists ( 'elysio_contacts_bar' ) ) {
	function elysio_contacts_bar(){
		$res = '';

		if( get_theme_mod( 'contacts_facebook' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_facebook' ) . '" target="_blank"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m437 0h-362c-41.351562 0-75 33.648438-75 75v362c0 41.351562 33.648438 75 75 75h151v-181h-60v-90h60v-61c0-49.628906 40.371094-90 90-90h91v90h-91v61h91l-15 90h-76v181h121c41.351562 0 75-33.648438 75-75v-362c0-41.351562-33.648438-75-75-75zm0 0"/></svg></a></li>';
		}

		if( get_theme_mod( 'contacts_instagram' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_instagram' ) . '" target="_blank"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m437 0h-362c-41.351562 0-75 33.648438-75 75v362c0 41.351562 33.648438 75 75 75h362c41.351562 0 75-33.648438 75-75v-362c0-41.351562-33.648438-75-75-75zm-180 390c-74.441406 0-135-60.558594-135-135s60.558594-135 135-135 135 60.558594 135 135-60.558594 135-135 135zm150-240c-24.8125 0-45-20.1875-45-45s20.1875-45 45-45 45 20.1875 45 45-20.1875 45-45 45zm0 0"/><path d="m407 90c-8.277344 0-15 6.722656-15 15s6.722656 15 15 15 15-6.722656 15-15-6.722656-15-15-15zm0 0"/><path d="m257 150c-57.890625 0-105 47.109375-105 105s47.109375 105 105 105 105-47.109375 105-105-47.109375-105-105-105zm0 0"/></svg></a></li>';
		}

		if( get_theme_mod( 'contacts_whatsapp' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_whatsapp' ) . '" target="_blank"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-140.609375 0-256 115.390625-256 256 0 46.40625 12.511719 91.582031 36.238281 131.105469l-36.238281 124.894531 124.894531-36.238281c39.523438 23.726562 84.699219 36.238281 131.105469 36.238281 140.609375 0 256-115.390625 256-256s-115.390625-256-256-256zm160.054688 364.167969-11.910157 11.910156c-16.851562 16.851563-55.605469 15.515625-80.507812 10.707031-82.800781-15.992187-179.335938-109.5625-197.953125-190.59375-9.21875-40.140625-4.128906-75.039062 9.183594-88.355468l11.910156-11.910157c6.574218-6.570312 17.253906-6.5625 23.820312 0l47.648438 47.652344c3.179687 3.179687 4.921875 7.394531 4.921875 11.90625s-1.742188 8.730469-4.921875 11.898437l-11.90625 11.921876c-13.125 13.15625-13.125 34.527343 0 47.652343l78.683594 77.648438c13.164062 13.164062 34.46875 13.179687 47.652343 0l11.910157-11.90625c6.148437-6.183594 17.632812-6.203125 23.832031 0l47.636719 47.636719c6.46875 6.441406 6.714843 17.113281 0 23.832031zm0 0"/></svg></a></li>';
		}

		if( get_theme_mod( 'contacts_youtube' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_youtube' ) . '" target="_blank"><svg viewBox="0 -61 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m437 0h-362c-41.351562 0-75 33.648438-75 75v240c0 41.351562 33.648438 75 75 75h362c41.351562 0 75-33.648438 75-75v-240c0-41.351562-33.648438-75-75-75zm-256 298.417969v-203.90625l180.496094 100.269531zm0 0"/><path d="m211 145.488281v101.105469l89.503906-51.375zm0 0"/></svg></a></li>';
		}
		
		if( get_theme_mod( 'contacts_twitter' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_twitter' ) . '" target="_blank"><svg viewBox="0 -47 512.00203 512" xmlns="http://www.w3.org/2000/svg"><path d="m191.011719 419.042969c-22.140625 0-44.929688-1.792969-67.855469-5.386719-40.378906-6.335938-81.253906-27.457031-92.820312-33.78125l-30.335938-16.585938 32.84375-10.800781c35.902344-11.804687 57.742188-19.128906 84.777344-30.597656-27.070313-13.109375-47.933594-36.691406-57.976563-67.175781l-7.640625-23.195313 6.265625.957031c-5.941406-5.988281-10.632812-12.066406-14.269531-17.59375-12.933594-19.644531-19.78125-43.648437-18.324219-64.21875l1.4375-20.246093 12.121094 4.695312c-5.113281-9.65625-8.808594-19.96875-10.980469-30.777343-5.292968-26.359376-.863281-54.363282 12.476563-78.851563l10.558593-19.382813 14.121094 16.960938c44.660156 53.648438 101.226563 85.472656 168.363282 94.789062-2.742188-18.902343-.6875-37.144531 6.113281-53.496093 7.917969-19.039063 22.003906-35.183594 40.722656-46.691407 20.789063-12.777343 46-18.96875 70.988281-17.433593 26.511719 1.628906 50.582032 11.5625 69.699219 28.746093 9.335937-2.425781 16.214844-5.015624 25.511719-8.515624 5.59375-2.105469 11.9375-4.496094 19.875-7.230469l29.25-10.078125-19.074219 54.476562c1.257813-.105468 2.554687-.195312 3.910156-.253906l31.234375-1.414062-18.460937 25.230468c-1.058594 1.445313-1.328125 1.855469-1.703125 2.421875-1.488282 2.242188-3.339844 5.03125-28.679688 38.867188-6.34375 8.472656-9.511718 19.507812-8.921875 31.078125 2.246094 43.96875-3.148437 83.75-16.042969 118.234375-12.195312 32.625-31.09375 60.617187-56.164062 83.199219-31.023438 27.9375-70.582031 47.066406-117.582031 56.847656-23.054688 4.796875-47.8125 7.203125-73.4375 7.203125zm0 0"/></svg></a></li>';
		}
		
		if( get_theme_mod( 'contacts_linkedin' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_linkedin' ) . '" target="_blank"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m437 0h-362c-41.351562 0-75 33.648438-75 75v362c0 41.351562 33.648438 75 75 75h362c41.351562 0 75-33.648438 75-75v-362c0-41.351562-33.648438-75-75-75zm-256 406h-60v-210h60zm0-240h-60v-60h60zm210 240h-60v-120c0-16.539062-13.460938-30-30-30s-30 13.460938-30 30v120h-60v-210h60v11.308594c15.71875-4.886719 25.929688-11.308594 45-11.308594 40.691406.042969 75 36.546875 75 79.6875zm0 0"/></svg></a></li>';
		}
		
		if( get_theme_mod( 'contacts_behance' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_behance' ) . '" target="_blank"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m181 181h-60v60h60c16.539062 0 30-13.460938 30-30s-13.460938-30-30-30zm0 0"/><path d="m181 271h-60v60h60c16.539062 0 30-13.460938 30-30s-13.460938-30-30-30zm0 0"/><path d="m346 241c-19.554688 0-36.238281 12.539062-42.4375 30h84.875c-6.199219-17.460938-22.882812-30-42.4375-30zm0 0"/><path d="m436 0h-361c-41.351562 0-75 33.648438-75 75v362c0 41.351562 33.648438 75 75 75h361c41.351562 0 76-33.648438 76-75v-362c0-41.351562-34.648438-75-76-75zm-150 151h120v30h-120zm-45 150c0 33.089844-26.910156 60-60 60h-90v-210h90c33.089844 0 60 26.910156 60 60 0 18.007812-8.132812 33.996094-20.730469 45 12.597657 11.003906 20.730469 26.992188 20.730469 45zm180 0h-117.4375c6.195312 17.460938 22.882812 30 42.4375 30 16.011719 0 30.953125-8.628906 38.992188-22.515625l25.957031 15.03125c-13.371094 23.113281-38.261719 37.484375-64.949219 37.484375-41.351562 0-75-33.648438-75-75s33.648438-75 75-75 75 33.648438 75 75zm0 0"/></svg></a></li>';
		}
		
		if( get_theme_mod( 'contacts_dribbble' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_dribbble' ) . '" target="_blank"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m392.425781 114.992188c15.664063-17.457032 24.796875-35.734376 28-54.496094-44.292969-37.089844-101.269531-60.496094-163.425781-60.496094-32.09375 0-62.699219 6.210938-91.011719 17.078125 47.410157 47.5 87.058594 103.742187 118.070313 164.453125 45.695312-16.523438 83.78125-39.128906 108.367187-66.539062zm0 0"/><path d="m414.75 135.03125c-27.519531 30.671875-68.425781 55.617188-117.296875 73.746094 6.074219 13.320312 11.65625 26.917968 16.910156 40.691406 25.097657-5.398438 51.109375-8.46875 77.636719-8.46875 41.761719 0 82.332031 7.085938 119.742188 20.109375.035156-1.714844.257812-3.382813.257812-5.109375 0-66.035156-25.445312-126.09375-66.773438-171.429688-6.097656 17.570313-16.21875 34.574219-30.476562 50.460938zm0 0"/><path d="m5.847656 202.113281c31.886719 5.636719 67.214844 8.886719 101.152344 8.886719 52.609375 0 103.144531-7.46875 148.183594-20.296875-31.035156-59.628906-70.933594-114.507813-118.554688-160.402344-64.808594 34.855469-114.8125 97.925781-130.78125 171.8125zm0 0"/><path d="m509.15625 291.996094c-36.1875-13.621094-76.015625-20.996094-117.15625-20.996094-23.0625 0-45.710938 2.417969-67.554688 6.832031 22.175782 66.503907 35.007813 138.386719 37.199219 211.453125 78.015625-35.292968 135.042969-109.632812 147.511719-197.289062zm0 0"/><path d="m0 256c0 78.890625 38.027344 150.511719 94.472656 197.320312 5.980469-39.785156 23.917969-78.925781 53.132813-111.683593 34.75-38.960938 82.972656-68.136719 137.582031-84.789063-5.082031-13.109375-10.632812-25.960937-16.492188-38.632812-48.796874 14.472656-103.757812 22.785156-161.695312 22.785156-35.445312 0-72.332031-3.386719-105.769531-9.261719-.757813 7.996094-1.230469 16.074219-1.230469 24.261719zm0 0"/><path d="m170.003906 361.601562c-29.511718 33.09375-45.5 71.90625-47.550781 111.613282 39.113281 24.425781 85.132813 38.785156 134.546875 38.785156 25.980469 0 51.046875-3.953125 74.683594-11.207031-1.148438-74.6875-13.6875-148.125-36.484375-215.71875-49.902344 14.964843-93.796875 41.320312-125.195313 76.527343zm0 0"/></svg></a></li>';
		}
		
		if( get_theme_mod( 'contacts_pinterest' ) ){
			$res .= '<li class="nav-item"><a class="nav-link active" href="' . get_theme_mod( 'contacts_pinterest' ) . '" target="_blank"><svg viewBox="-62 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m60.945312 278.21875c.640626-1.597656 7.050782-26.230469 7.5-27.898438-10.007812-15.058593-21.125-64.371093-12.597656-104.398437 9.199219-58.730469 71.4375-87.601563 130.199219-87.601563v-.109374c73.570313.046874 128.640625 40.980468 128.699219 111.476562.046875 55.179688-33.195313 128.117188-89.957032 128.117188-.015624 0-.027343 0-.042968 0-20.257813 0-45.90625-9.1875-52.632813-18.210938-7.761719-10.398438-9.667969-23.230469-5.566406-36.941406 10.050781-32.082032 22.867187-70.511719 24.363281-96.136719 1.386719-24.183594-15.773437-39.917969-38.027344-39.917969-16.746093 0-38.496093 9.726563-49.335937 37.058594-8.953125 22.707031-8.761719 46.480469.585937 72.671875 3.644532 10.238281-16.15625 76.984375-22.5 98.71875-15.761718 53.992187-37.339843 122.304687-32.726562 160.347656l4.453125 36.605469 22.367187-29.3125c30.953126-40.519531 62.957032-145.332031 71.484376-170.835938 25.210937 32.648438 77.710937 33.585938 83.832031 33.585938 75.183593 0 160.4375-74.65625 158.019531-178.5625-2.121094-91.121094-68.808594-166.875-188.632812-166.875v.117188c-113.976563 0-180.5 60.835937-196.785157 137.703124-14.914062 71.273438 18.253907 125.519532 57.300781 140.398438zm0 0"/></svg></a></li>';
		}

		echo '<ul class="nav elysio-contacts-bar mt-3 mt-md-0 justify-content-md-end">' . $res . '</ul>';
	}
}
