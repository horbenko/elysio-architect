<?php

// Kirki::add_panel( 'contacts', array(
//     'priority'    => 10,
//     'title'       => esc_html__( 'Contacts Info', 'elysio' ),
// ) );

Kirki::add_section( 'contacts', array(
    'title'          => esc_html__( 'Contacts Info', 'elysio' ),
    'panel'          => '',
    'priority'       => 10,
) );

Kirki::add_field( 'elysio_config', [
	'type'     => 'email',
	'settings' => 'contacts_email',
	'label'    => esc_html__( 'Contacts Email', 'elysio' ),
	'section'  => 'contacts',
	'default'  => esc_html__( 'example@mail.com', 'elysio' ),
	'priority' => 10,
] );

Kirki::add_field( 'elysio_config', [
	'type'     => 'tel',
	'settings' => 'contacts_tel',
	'label'    => esc_html__( 'Contacts Phone', 'elysio' ),
	'section'  => 'contacts',
	'default'  => esc_html__( '+1500 03 04 567', 'elysio' ),
	'priority' => 10,
] );

Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_facebook',
	'label'    => __( 'Facebook Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://facebook.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_instagram',
	'label'    => __( 'Instagram Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://instagram.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_whatsapp',
	'label'    => __( 'WhatsApp Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://whatsapp.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_youtube',
	'label'    => __( 'YouTube Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://youtube.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_twitter',
	'label'    => __( 'Twitter Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://twitter.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_linkedin',
	'label'    => __( 'LinkedIn Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://linkedin.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_behance',
	'label'    => __( 'Behance Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://behance.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_dribbble',
	'label'    => __( 'Dribbble Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://dribbble.com/',
	'priority' => 10,
] );
Kirki::add_field( 'elysio_config', [
	'type'     => 'link',
	'settings' => 'contacts_pinterest',
	'label'    => __( 'Pinterest Link', 'elysio' ),
	'section'  => 'contacts',
	'default'  => 'https://pinterest.com/',
	'priority' => 10,
] );


