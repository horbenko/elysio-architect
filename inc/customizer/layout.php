<?php

/**
 * Elysio Theme Customizer - Layout [TOFIX]
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_layout' ) ) {
	function elysio_theme_customize_layout( $wp_customize ) {

		/**
		 * Add Theme Layout Section
		 */
		$wp_customize->add_section(
			'theme_layout_options',
			array(
				'title'       => __( 'Theme Layout', 'elysio-architect' ),
				'capability'  => 'edit_theme_options',
				'description' => __( 'Theme Layout Settings', 'elysio-architect' ),
			)
		);

		/**
		 * Container Width
		 */
		$wp_customize->add_setting(
			'elysio_container_type',
			array(
				'default'           => 'container',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'elysio_theme_slug_sanitize_select',
				'capability'        => 'edit_theme_options',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'elysio_container_type',
				array(
					'label'       => __( 'Container Width', 'elysio-architect' ),
					'description' => __( 'Choose between Bootstrap\'s container and container-fluid', 'elysio-architect' ),
					'section'     => 'theme_layout_options',
					'settings'    => 'elysio_container_type',
					'type'        => 'select',
					'choices'     => array(
						'container'       => __( 'Fixed width container', 'elysio-architect' ),
						'container-fluid' => __( 'Full width container', 'elysio-architect' ),
					),
				)
			)
		);

		/**
		 * Sidebar Position
		 */
		$wp_customize->add_setting(
			'elysio_sidebar_position',
			array(
				'default'           => 'right',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'elysio_sidebar_position',
				array(
					'label'             => __( 'Blog Sidebar Positioning', 'elysio-architect' ),
					'description'       => __(
						'Set sidebar\'s default position. Can either be: right, left or none.',
						'elysio-architect'
					),
					'section'           => 'theme_layout_options',
					'settings'          => 'elysio_sidebar_position',
					'type'              => 'select',
					'sanitize_callback' => 'elysio_theme_slug_sanitize_select',
					'choices'           => array(
						'right' => __( 'Right sidebar', 'elysio-architect' ),
						'left'  => __( 'Left sidebar', 'elysio-architect' ),
						'none'  => __( 'No sidebar', 'elysio-architect' ),
					),
				)
			)
		);

	}
}
add_action( 'customize_register', 'elysio_theme_customize_layout' );