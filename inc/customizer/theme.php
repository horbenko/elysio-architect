<?php

/**
 * Elysio Theme Customizer - Theme
 *
 * @author artem.horbenko
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_theme' ) ) {
	function elysio_theme_customize_theme( $wp_customize ) {

		/**
		 * Add Theme Settings Section
		 */
		$wp_customize->add_section(
			'elysio_theme_settings',
			array(
				'title'       => __( 'Theme Setting', 'elysio-architect' ),
				'capability'  => 'edit_theme_options',
			)
		);

		/**
		 * Google Maps API Key
		 */
		$wp_customize->add_setting( 'google_maps_api',
			array(
				'default'	=> '',
				'transport'	=> 'refresh',
			)
		);
		$wp_customize->add_control( 'google_maps_api',
			array(
				'label' => __( 'Google Maps API Key', 'elysio-architect' ),
				'description'	=> '<a href="https://developers.google.com/maps/documentation/javascript">Create an API key</a>',
				'section' => 'elysio_theme_settings',
				'type' => 'text',
			)
		);

	}
}
add_action( 'customize_register', 'elysio_theme_customize_theme' );