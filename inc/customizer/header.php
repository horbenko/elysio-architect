<?php

/**
 * Elysio Theme Customizer - Header 
 *

  Colors:
  + Bg
  + Text 
  + Primary (Link & search, humb and etc.)
  + Hover

 * @package elysio-architect
 */


if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}




if ( ! function_exists( 'elysio_theme_customize_header' ) ) {

  function elysio_theme_customize_header( $wp_customize ) {

    /**
     * Add Site Header Panel
     */
     $wp_customize->add_panel( 'site_header_panel',
       array(
          'title' => __( 'Site Header', 'elysio-architect' ),
          'description' => __( 'A website header sits at the top of each page and serves a few very important purposes. Adjust your Header and Navigation sections.', 'elysio-architect' ), // Include html tags such as 
          'priority' => 120, // Not typically needed. Default is 160
          'capability' => 'edit_theme_options', // Not typically needed. Default is edit_theme_options
          'theme_supports' => '', // Rarely needed
          'active_callback' => '', // Rarely needed
       )
    );

    /**
     * Add Header Styles Section
     */
    $wp_customize->add_section( 'header_style_section',
       array(
          'title' => __( 'Header Styles', 'elysio-architect' ),
          // 'description' => esc_html__( 'These are an example of Customizer Custom Controls.' ),
          'panel' => 'site_header_panel', // Only needed if adding your Section to a Panel
          'priority' => 160, // Not typically needed. Default is 160
          'capability' => 'edit_theme_options', // Not typically needed. Default is edit_theme_options
          'theme_supports' => '', // Rarely needed
          'active_callback' => '', // Rarely needed
          'description_hidden' => 'false', // Rarely needed. Default is False
       )
    );

     // Header Colors
     $wp_customize->add_setting( 'header_bgcolor',
      array(
        'default'    => '#ffffff',
        'type'       => 'theme_mod',
        'capability' => 'edit_theme_options',
        'transport'   => 'refresh',
      )
     );
     $wp_customize->add_control( new WP_Customize_Color_Control(
      $wp_customize,
      'elysio_header-bg-color',
        array(
          'label'      => __( 'Header Background Color', 'elysio-architect' ),
          'settings'   => 'header_bgcolor',
          'section'    => 'header_style_section',
        ) 
     ) );
     $wp_customize->add_setting( 'header_text_color',
      array(
        'default'    => '#888888',
        'type'       => 'theme_mod',
        'capability' => 'edit_theme_options',
        'transport'   => 'refresh',
      )
     );
     $wp_customize->add_control( new WP_Customize_Color_Control(
      $wp_customize,
      'elysio_header-text-color',
        array(
          'label'      => __( 'Header Text Color', 'elysio-architect' ),
          'settings'   => 'header_text_color',
          'section'    => 'header_style_section',
        ) 
     ) );

     $wp_customize->add_setting( 'header_link_color',
      array(
        'default'    => '#888888',
        'type'       => 'theme_mod',
        'capability' => 'edit_theme_options',
        'transport'   => 'refresh',
      )
     );
     $wp_customize->add_control( new WP_Customize_Color_Control(
      $wp_customize,
      'elysio_header-link-color',
        array(
          'label'      => __( 'Header Link Color', 'elysio-architect' ),
          'settings'   => 'header_link_color',
          'section'    => 'header_style_section',
        ) 
     ) );
     $wp_customize->add_setting( 'header_link_hover_color',
      array(
        'default'    => '#000000',
        'type'       => 'theme_mod',
        'capability' => 'edit_theme_options',
        'transport'   => 'refresh',
      )
     );
     $wp_customize->add_control( new WP_Customize_Color_Control(
      $wp_customize,
      'elysio_header-link-hover-color',
        array(
          'label'      => __( 'Header Link Hover Color', 'elysio-architect' ),
          'settings'   => 'header_link_hover_color',
          'section'    => 'header_style_section',
        ) 
     ) );
  }
}
add_action( 'customize_register', 'elysio_theme_customize_header' );

function header_customize_css()
{
  ?>
  <style type="text/css">

    /* Site Header - Customizer */
    .elysio-navbar {
      background-color: <?php echo get_theme_mod('header_bgcolor', 'white'); ?>;
      color: <?php echo get_theme_mod('header_text_color', 'rgba(0,0,0,.5)'); ?>;
    }
    .main-navigation ul ul {
      background-color: <?php echo get_theme_mod('header_bgcolor', 'white'); ?>;
    }


    /* Branding */
    .elysio-navbar .navbar-brand a {
      color: <?php echo get_theme_mod('header_link_color', '#BD8881'); ?>;
    }
    .elysio-navbar .navbar-brand a:hover {
      color: <?php echo get_theme_mod('header_link_hover_color', 'rgba(0,0,0,.7)'); ?>;
    }
   

    /* Primary Nav Menu */
    .elysio-navbar .navbar-nav a {
      color: <?php echo get_theme_mod('header_link_color', '#BD8881'); ?>;
    }
    .elysio-navbar .navbar-nav a:hover,
    .elysio-navbar .navbar-nav a:focus,
    .elysio-navbar .navbar-nav active > a
    {
      color: <?php echo get_theme_mod('header_link_hover_color', 'rgba(0,0,0,.7)'); ?>;
    }
    .elysio-navbar .dropdown-menu {
      background-color: <?php echo get_theme_mod('header_bgcolor', 'white'); ?>;
    }


    /* Nav Menu Toggler */
    .elysio-navbar .navbar-toggler {
      fill: <?php echo get_theme_mod('header_link_color', '#BD8881'); ?>;
    }


    /* Header Search Button */
    .elysio-navbar .header_search-icon {
      fill: <?php echo get_theme_mod('header_link_color', '#BD8881'); ?>;
    }
    .elysio-navbar .header_search-icon:hover {
      fill: <?php echo get_theme_mod('header_link_hover_color', 'rgba(0,0,0,.7)'); ?>;
    }

  </style>
  <?php
}
add_action( 'wp_head', 'header_customize_css');