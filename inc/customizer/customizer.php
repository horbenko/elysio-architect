<?php
/**
 * Elysio Theme Customizer [TODO]
 *
 * @package elysio-architect
 */

// if ( ! defined( 'ABSPATH' ) ) {
// 	exit; // Exit if accessed directly.
// }

// /**
//  * Add postMessage support for site title and description for the Theme Customizer.
//  *
//  * @param WP_Customize_Manager $wp_customize Theme Customizer object.
//  */
// if ( ! function_exists( 'elysio_customize_register' ) ) {
// 	/**
// 	 * Register basic customizer support.
// 	 *
// 	 * @param object $wp_customize Customizer reference.
// 	 */
// 	function elysio_customize_register( $wp_customize ) {
// 		$wp_customize->get_setting( 'blogname' )->transport         = 'refresh';
// 		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
// 		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
// 		$wp_customize->get_setting( 'background_color' )->transport = 'postMessage';

// 	}
// }
// add_action( 'customize_register', 'elysio_customize_register' );

// if ( ! function_exists( 'understrap_theme_customize_register' ) ) {
// 	/**
// 	 * Register individual settings through customizer's API.
// 	 *
// 	 * @param WP_Customize_Manager $wp_customize Customizer reference.
// 	 */
// 	function understrap_theme_customize_register( $wp_customize ) {

// 		// Theme Layout Section
		// $wp_customize->add_section(
		// 	'theme_layout_options',
		// 	array(
		// 		'title'       => __( 'Layout', 'elysio-architect' ),
		// 		'capability'  => 'edit_theme_options',
		// 		'description' => __( 'Theme Layout Settings', 'elysio-architect' ),
		// 		'priority'    => 160,
		// 	)
		// );

// 		// Theme Fonts Section
// 		$wp_customize->add_section(
// 			'elysio_theme_fonts_options',
// 			array(
// 				'title'       => __( 'Fonts', 'elysio-architect' ),
// 				'capability'  => 'edit_theme_options',
// 				// 'description' => __( 'Container width and sidebar defaults', 'elysio-architect' ),
// 				'priority'    => 40,
// 			)
// 		);

// 		/**
// 		 * Add our Sample Section
// 		 */
// 		$wp_customize->add_section( 'sample_custom_controls_section',
// 		   array(
// 		      'title' => __( 'Sample Custom Controls' ),
// 		      'description' => esc_html__( 'These are an example of Customizer Custom Controls.' ),
// 		      'panel' => '', // Only needed if adding your Section to a Panel
// 		      'priority' => 160, // Not typically needed. Default is 160
// 		      'capability' => 'edit_theme_options', // Not typically needed. Default is edit_theme_options
// 		      'theme_supports' => '', // Rarely needed
// 		      'active_callback' => '', // Rarely needed
// 		      'description_hidden' => 'false', // Rarely needed. Default is False
// 		   )
// 		);

		/**
		 * Select sanitization function
		 *
		 * @param string               $input   Slug to sanitize.
		 * @param WP_Customize_Setting $setting Setting instance.
		 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
		 */
// 		function understrap_theme_slug_sanitize_select( $input, $setting ) {

// 			// Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
// 			$input = sanitize_key( $input );

// 			// Get the list of possible select options.
// 			$choices = $setting->manager->get_control( $setting->id )->choices;

// 				// If the input is a valid key, return it; otherwise, return the default.
// 				return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

// 		}

// 		$wp_customize->add_setting( 'sample_default_text',
// 		   array(
// 		      'default' => '',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'skyrocket_text_sanitization'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_text',
// 		   array(
// 		      'label' => __( 'Default Text Control' ),
// 		      'description' => esc_html__( 'Text controls Type can be either text, email, url, number, hidden, or date' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type' => 'text', // Can be either text, email, url, number, hidden, or date
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		      'input_attrs' => array( // Optional.
// 		         'class' => 'my-custom-class',
// 		         'style' => 'border: 1px solid rebeccapurple',
// 		         'placeholder' => __( 'Enter name...' ),
// 		      ),
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_checkbox',
// 		   array(
// 		      'default' => 0,
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'skyrocket_switch_sanitization'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_checkbox',
// 		   array(
// 		      'label' => __( 'Default Checkbox Control', 'ephemeris' ),
// 		      'description' => esc_html__( 'Sample description' ),
// 		      'section'  => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type'=> 'checkbox',
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_select',
// 		   array(
// 		      'default' => 'jet-fuel',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'skyrocket_radio_sanitization'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_select',
// 		   array(
// 		      'label' => __( 'Standard Select Control' ),
// 		      'description' => esc_html__( 'Sample description' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type' => 'select',
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		      'choices' => array( // Optional.
// 		         'wordpress' => __( 'WordPress' ),
// 		         'hamsters' => __( 'Hamsters' ),
// 		         'jet-fuel' => __( 'Jet Fuel' ),
// 		         'nuclear-energy' => __( 'Nuclear Energy' )
// 		      )
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_radio',
// 		   array(
// 		      'default' => 'spider-man',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'skyrocket_radio_sanitization'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_radio',
// 		   array(
// 		      'label' => __( 'Standard Radio Control' ),
// 		      'description' => esc_html__( 'Sample description' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type' => 'radio',
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		      'choices' => array( // Optional.
// 		         'captain-america' => __( 'Captain America' ),
// 		         'iron-man' => __( 'Iron Man' ),
// 		         'spider-man' => __( 'Spider-Man' ),
// 		         'thor' => __( 'Thor' )
// 		      )
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_dropdownpages',
// 		   array(
// 		      'default' => '1548',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'absint'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_dropdownpages',
// 		   array(
// 		      'label' => __( 'Default Dropdown Pages Control' ),
// 		      'description' => esc_html__( 'Sample description' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type' => 'dropdown-pages',
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_textarea',
// 		   array(
// 		      'default' => '',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'wp_filter_nohtml_kses'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_textarea',
// 		   array(
// 		      'label' => __( 'Default Textarea Control' ),
// 		      'description' => esc_html__( 'Sample description' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type' => 'textarea',
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		      'input_attrs' => array( // Optional.
// 		         'class' => 'my-custom-class',
// 		         'style' => 'border: 1px solid #999',
// 		         'placeholder' => __( 'Enter message...' ),
// 		      ),
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_color',
// 		   array(
// 		      'default' => '#333',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'sanitize_hex_color'
// 		   )
// 		);
// 		$wp_customize->add_control( 'sample_default_color',
// 		   array(
// 		      'label' => __( 'Default Color Control' ),
// 		      'description' => esc_html__( 'Sample description' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'priority' => 10, // Optional. Order priority to load the control. Default: 10
// 		      'type' => 'color',
// 		      'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
// 		   )
// 		);

// 		$wp_customize->add_setting( 'sample_default_media',
// 		   array(
// 		      'default' => '',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'absint'
// 		   )
// 		);
// 		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'sample_default_media',
// 		   array(
// 		      'label' => __( 'Default Media Control' ),
// 		      'description' => esc_html__( 'This is the description for the Media Control' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'mime_type' => 'image',  // Required. Can be image, audio, video, application, text
// 		      'button_labels' => array( // Optional
// 		         'select' => __( 'Select File' ),
// 		         'change' => __( 'Change File' ),
// 		         'default' => __( 'Default' ),
// 		         'remove' => __( 'Remove' ),
// 		         'placeholder' => __( 'No file selected' ),
// 		         'frame_title' => __( 'Select File' ),
// 		         'frame_button' => __( 'Choose File' ),
// 		      )
// 		   )
// 		) );

// 		$wp_customize->add_setting( 'sample_default_image',
// 		   array(
// 		      'default' => '',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'absint'
// 		   )
// 		);
// 		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sample_default_image',
// 		   array(
// 		      'label' => __( 'Default Image Control' ),
// 		      'description' => esc_html__( 'This is the description for the Image Control' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'button_labels' => array( // Optional.
// 		         'select' => __( 'Select Image' ),
// 		         'change' => __( 'Change Image' ),
// 		         'remove' => __( 'Remove' ),
// 		         'default' => __( 'Default' ),
// 		         'placeholder' => __( 'No image selected' ),
// 		         'frame_title' => __( 'Select Image' ),
// 		         'frame_button' => __( 'Choose Image' ),
// 		      )
// 		   )
// 		) );

// 		$wp_customize->add_setting( 'sample_default_cropped_image',
// 		   array(
// 		      'default' => '',
// 		      'transport' => 'refresh',
// 		      'sanitize_callback' => 'absint'
// 		   )
// 		);
// 		$wp_customize->add_control( new WP_Customize_Cropped_Image_Control( $wp_customize, 'sample_default_cropped_image',
// 		   array(
// 		      'label' => __( 'Default Cropped Image Control' ),
// 		      'description' => esc_html__( 'This is the description for the Cropped Image Control' ),
// 		      'section' => 'sample_custom_controls_section',
// 		      'flex_width' => false, // Optional. Default: false
// 		      'flex_height' => true, // Optional. Default: false
// 		      'width' => 800, // Optional. Default: 150
// 		      'height' => 400, // Optional. Default: 150
// 		      'button_labels' => array( // Optional.
// 		         'select' => __( 'Select Image' ),
// 		         'change' => __( 'Change Image' ),
// 		         'remove' => __( 'Remove' ),
// 		         'default' => __( 'Default' ),
// 		         'placeholder' => __( 'No image selected' ),
// 		         'frame_title' => __( 'Select Image' ),
// 		         'frame_button' => __( 'Choose Image' ),
// 		      )
// 		   )
// 		) );

// 	}
// } // endif function_exists( 'understrap_theme_customize_register' ).
// add_action( 'customize_register', 'understrap_theme_customize_register' );

// /**
//  * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
//  */
// if ( ! function_exists( 'understrap_customize_preview_js' ) ) {
// 	/**
// 	 * Setup JS integration for live previewing.
// 	 */
// 	function understrap_customize_preview_js() {
// 		wp_enqueue_script(
// 			'understrap_customizer',
// 			get_template_directory_uri() . '/js/customizer.js',
// 			array( 'customize-preview' ),
// 			'20130508',
// 			true
// 		);
// 	}
// }
// add_action( 'customize_preview_init', 'understrap_customize_preview_js' );

// function mytheme_customize_css()
// {

// }
// add_action( 'wp_head', 'mytheme_customize_css');