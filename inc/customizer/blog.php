<?php

/**
 * Elysio Theme Customizer - Blog [TODO]
 *
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_blog' ) ) {

	function elysio_theme_customize_blog( $wp_customize ) {

			$wp_customize->add_section(
			  'elysio_theme_blog',
			  array(
				  'title'       => __( 'Blog', 'elysio-architect' ),
				  'capability'  => 'edit_theme_options',

			  )
			);



		  	$wp_customize->add_setting( 'blog_caption',
		  		array(
		  			'default'    => '#fff',
		  			'type'       => 'theme_mod',
		  			'capability' => 'edit_theme_options',
		  			'transport'   => 'refresh',
		  		)
		  	);
			$wp_customize->add_control(
				new Customizer_Library_Help_Text(
				  $wp_customize,
				  'blog_caption', array(
				      'label'             => 'Blog & Archives',
				      'section'           => 'elysio_theme_blog',
				      // 'description'       => 'Allows users to smoothly scroll back to the top of the page.'
				  )
				)
			);
			$wp_customize->add_setting( 'blog_title_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_title_checkbox',
				array(
				   'label' => __( 'Hide Blog Title ', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);
			$wp_customize->add_setting( 'blog_meta_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_meta_checkbox',
				array(
				   'label' => __( 'Hide Blog Meta ', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);
			$wp_customize->add_setting( 'blog_footer_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_footer_checkbox',
				array(
				   'label' => __( 'Hide Blog Categories & Tags', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);



		  	$wp_customize->add_setting( 'blog_single_caption',
		  		array(
		  			'default'    => '#fff',
		  			'type'       => 'theme_mod',
		  			'capability' => 'edit_theme_options',
		  			'transport'   => 'refresh',
		  		)
		  	);
			$wp_customize->add_control(
				new Customizer_Library_Help_Text(
				  $wp_customize,
				  'blog_single_caption', array(
				      'label'             => 'Single Post',
				      'section'           => 'elysio_theme_blog',
				      // 'description'       => 'Allows users to smoothly scroll back to the top of the page.'
				  )
				)
			);
			$wp_customize->add_setting( 'blog_single_meta_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_single_meta_checkbox',
				array(
				   'label' => __( 'Hide Single Post Meta ', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);
			$wp_customize->add_setting( 'blog_single_footer_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_single_footer_checkbox',
				array(
				   'label' => __( 'Hide Single Post Cats & Tags', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);
			$wp_customize->add_setting( 'blog_single_singlenav_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_single_singlenav_checkbox',
				array(
				   'label' => __( 'Hide Next, Prev Navigation', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);
			$wp_customize->add_setting( 'blog_single_related_checkbox',
				array(
				   'default' => 0,
				   'transport' => 'refresh',
				)
			);
			$wp_customize->add_control( 'blog_single_related_checkbox',
				array(
				   'label' => __( 'Hide Related Posts', 'elysio-architect' ),
				   // 'description' => esc_html__( 'Allows users to smoothly scroll back to the top of the page.' ),
				   'section'  => 'elysio_theme_blog',
				   'type'=> 'checkbox',
				)
			);
		  
	}

}
add_action( 'customize_register', 'elysio_theme_customize_blog' );

function blog_customize_css()
{
    ?>
         <style type="text/css">



         	<?php if( get_theme_mod( 'blog_title_checkbox' ) ){ ?>
				body.archive .page-header{
					display: none;
				}
			<?php } ?>
         	<?php if( get_theme_mod( 'blog_meta_checkbox' ) ){ ?>
				body.blog .entry-meta,
				body.archive .entry-meta
				{
					display: none;
				}
			<?php } ?>
         	<?php if( get_theme_mod( 'blog_footer_checkbox' ) ){ ?>
				body.blog .entry-footer,
				body.archive .entry-footer
				{
					display: none;
				}
			<?php } ?>


         	<?php if( get_theme_mod( 'blog_single_meta_checkbox' ) ){ ?>
				body.single-post .entry-meta {
					display: none;
				}
			<?php } ?>
         	<?php if( get_theme_mod( 'blog_single_footer_checkbox' ) ){ ?>
				body.single-post .entry-footer {
					display: none;
				}
			<?php } ?>
         	<?php if( get_theme_mod( 'blog_single_related_checkbox' ) ){ ?>
				body.single-post .elysio-related-posts,
				body.elysio_portfolio-template-default .elysio-related-projects
				{
					display: none;
				}
			<?php } ?>
         	<?php if( get_theme_mod( 'blog_single_singlenav_checkbox' ) ){ ?>
				body.single-post .post-navigation,
				body.elysio_portfolio-template-default .post-navigation
				{
					display: none;
				}
			<?php } ?>
     	</style>
     <?php
 }
 add_action( 'wp_head', 'blog_customize_css');