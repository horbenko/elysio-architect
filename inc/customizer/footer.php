<?php

/**
 * Elysio Theme Customizer - Footer Layout
 *
 * @author artem.horbenko
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_footer' ) ) {
	function elysio_theme_customize_footer( $wp_customize ) {

		/**
		 * Add Site Footer Panel
		 */
		$wp_customize->add_panel( 'site_footer_panel',
			array(
				'title'			=> __( 'Site Footer', 'elysio-architect' ),
				'capability'	=> 'edit_theme_options',
				'description'	=> 'A website footer is found at the bottom of your site pages. It typically includes important information such as a copyright notice, a disclaimer, or a few links to relevant resources.',
			)
		);
		
		/**
		 * Add Site Footer Layout Section
		 */
		$wp_customize->add_section( 'footer_layout_section',
			array(
				'title'			=> __( 'Footer Layout', 'elysio-architect' ),
				'panel'			=> 'site_footer_panel', 
				'capability'	=> 'edit_theme_options',
			)
		);

		/**
		 * Site Footer Copyright
		 */
		$wp_customize->add_setting( 'footer_copyright',
			array(
				'default' => '© Copyright',
				'transport' => 'refresh',
			)
		);
		$wp_customize->add_control( 'footer_copyright',
			array(
				'label' => __( 'Copyright Text', 'elysio-architect' ),
				'description' => __( 'Having a copyright footer on your site is probably a good idea.', 'elysio-architect' ),
				'section' => 'footer_layout_section',
				'type' => 'text', // Can be either text, email, url, number, hidden, or date
				'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
				// 'input_attrs' => array( // Optional.
				//    'class' => 'my-custom-class',
				//    'style' => 'border: 1px solid rebeccapurple',
				//    'placeholder' => __( 'Enter name...' ),
				// ),
			)
		);
		/**
		 * Container Width
		 */
		$wp_customize->add_setting(
			'footer_copyright_align',
			array(
				'default'           => 'left',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'elysio_footer_copyright_align',
				array(
					'label'       => __( 'Copyright Text Align', 'elysio-architect' ),
					'section'     => 'footer_layout_section',
					'settings'    => 'footer_copyright_align',
					'type'        => 'select',
					'choices'     => array(
						'left'		=> __( 'Left', 'elysio-architect' ),
						'center'	=> __( 'Center', 'elysio-architect' ),
						'right'		=> __( 'Right', 'elysio-architect' ),
					),
				)
			)
		);
		/**
		 * Site Footer Bump
		 */
		$wp_customize->add_setting( 'footer_bump_checkbox',
			array(
				'default' => 0,
				'transport' => 'refresh',
			)
		);
		$wp_customize->add_control( 'footer_bump_checkbox',
			array(
				'label' => __( 'Show Back-to-Top Button', 'elysio-architect' ),
				'description' => __( 'Allows users to smoothly scroll back to the top of the page.', 'elysio-architect' ),
				'section'  => 'footer_layout_section',
				'type'=> 'checkbox',
			)
		);

	}
}
add_action( 'customize_register', 'elysio_theme_customize_footer' );