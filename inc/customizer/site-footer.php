<?php

Kirki::add_panel( 'site_footer', array(
    'priority'    => 10,
    'title'       => esc_html__( 'Site Footer 2', 'elysio' ),
) );

Kirki::add_section( 'site_footer_colors', array(
    'title'          => esc_html__( 'Footer Colors', 'elysio' ),
    'panel'          => 'site_footer',
    'priority'       => 10,
) );


Kirki::add_field( 'elysio_config', [
	'type'        => 'color',
	'settings'    => 'site_footer_background',
	'label'       => __( 'Background', 'elysio' ),
	'section'     => 'site_footer_colors',
	'default'     => '#111',
	'choices'     => [
		'alpha' => true,
	],
	'output' => array(
		array(
			'element'  => '.site-footer',
			'property' => 'background-color',
		),
	),
	'transport' => 'auto',
] );

Kirki::add_field( 'elysio_config', [
	'type'        => 'color',
	'settings'    => 'site_footer_text_color',
	'label'       => __( 'Text Color', 'elysio' ),
	'section'     => 'site_footer_colors',
	'default'     => '#F8F9F9',
	'choices'     => [
		'alpha' => true,
	],
	'output' => array(
		array(
			'element'  => '.site-footer',
			'property' => 'color',
		),
		/* Elysio Site Footer Searchform Widget  */
		array(
			'element'  => '.site-footer form.material-form input',
			'property' => 'color',
		),
		/* Elysio Site Footer Newsletter Widget  */
		array(
			'element'  => '.site-footer .widget_elysio_newsletter_mailchimp_widget .mc-field-group input',
			'property' => 'color',
		),
		/* Elysio Site Footer Contactform7 */
		array(
			'element'  => '.site-footer .form-control',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .wpcf7 .wpcf7-validation-errors',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .wpcf7 input',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .wpcf7 textarea',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .wpcf7 select',
			'property' => 'color',
		),
	),
	'transport' => 'auto',
] );

Kirki::add_field( 'elysio_config', [
	'type'        => 'color',
	'settings'    => 'site_footer_primary_color',
	'label'       => __( 'Primary Color', 'elysio' ),
	'description' => __( 'Color for links, buttons, focus and etc.', 'elysio' ),
	'section'     => 'site_footer_colors',
	'default'     => '#fff',
	'choices'     => [
		'alpha' => true,
	],
	'output' => array(
		array(
			'element'  => '.site-footer a',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .footer-widget li a',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .site-info .elysio-contacts-bar svg',
			'property' => 'fill',
		),
		/* Elysio Site Footer Searchform Widget  */
		array(
			'element'  => '.site-footer form.material-form .bar:before',
			'property' => 'background-color',
		),
		array(
			'element'  => '.site-footer form.material-form input:valid~label',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer form.material-form input:focus~label',
			'property' => 'color',
		),
		/* Elysio Site Footer Newsletter Widget */
		array(
			'element'  => '.site-footer .widget_elysio_newsletter_mailchimp_widget .mc-field-group input:focus',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .widget_elysio_newsletter_mailchimp_widget .mc-field-group input:focus~label',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .widget_elysio_newsletter_mailchimp_widget .mc-field-group input:valid~label',
			'property' => 'color',
		),
		/* Elysio Site Footer Contactform */
		array(
			'element'  => '.site-footer .wpcf7 input:focus',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 textarea:focus',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 select:focus',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 input[type=submit]',
			'property' => 'color',
		),
	),
	'transport' => 'auto',
] );

Kirki::add_field( 'elysio_config', [
	'type'        => 'color',
	'settings'    => 'site_footer_decorations_color',
	'label'       => __( 'Decorations Color', 'elysio' ),
	'description' => __( 'Color for input field, labels, lines, borders and etc.', 'elysio' ),
	'section'     => 'site_footer_colors',
	'default'     => '#ccc',
	'choices'     => [
		'alpha' => true,
	],
	'output' => array(
		array(
			'element'  => '.site-footer caption',
			'property' => 'color',
		),
		/* Elysio Site Footer Searchform Widget  */
		array(
			'element'  => '.site-footer form.material-form label',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer form.material-form input',
			'property' => 'border-bottom-color',
		),
		/* Elysio Site Footer Newsletter Widget */
		array(
			'element'  => '.site-footer .widget_elysio_newsletter_mailchimp_widget .mc-field-group label',
			'property' => 'color',
		),
		array(
			'element'  => '.site-footer .widget_elysio_newsletter_mailchimp_widget .mc-field-group input',
			'property' => 'border-bottom-color',
		),
		array(
			'element'  => '.site-footer .mc-field-group .button svg',
			'property' => 'fill',
		),
		/* Elysio Site Footer Contactform */
		array(
			'element'  => '.site-footer .form-control',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 .wpcf7-validation-errors',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 input',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 textarea',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 select',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 input[type=submit]',
			'property' => 'border-color',
		),
		array(
			'element'  => '.site-footer .wpcf7 input[type=submit]',
			'property' => 'background-color',
		),

	),
	'transport' => 'auto',
] );


/* Elysio Site Footer Bump */

Kirki::add_field( 'elysio_config', [
	'type'        => 'custom',
	'settings'    => 'bump_hr',
	'label'       => '',
	'section'     => 'site_footer_colors',
	'default'     => '<hr>',
	'priority'    => 10,
] );

Kirki::add_field( 'elysio_config', [
	'type'        => 'color',
	'settings'    => 'site_footer_bump_arrow_color',
	'label'       => __( 'Bump Arrow', 'elysio' ),
	'section'     => 'site_footer_colors',
	'default'     => '#111',
	'choices'     => [
		'alpha' => true,
	],
	'output' => array(
		array(
			'element'  => '.site-footer .bump .bump-icon-path',
			'property' => 'fill',
		),
	),
	'transport' => 'auto',
] );

Kirki::add_field( 'elysio_config', [
	'type'        => 'color',
	'settings'    => 'site_footer_bump_background',
	'label'       => __( 'Bump Background', 'elysio' ),
	'section'     => 'site_footer_colors',
	'default'     => '#fff',
	'choices'     => [
		'alpha' => true,
	],
	'output' => array(
		array(
			'element'  => '.site-footer .bump',
			'property' => 'background-color',
		),
	),
	'transport' => 'auto',
] );