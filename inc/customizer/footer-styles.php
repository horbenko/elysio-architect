<?php

/**
 * Elysio Theme Customizer - Footer Styles
 *
 * @author artem.horbenko
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_footer_styles' ) ) {
	function elysio_theme_customize_footer_styles( $wp_customize ) {

		/**
		 * Add Site Footer Styles Section
		 */
		$wp_customize->add_section( 'footer_style_section',
			array(
				'title'	=> __( 'Footer Styles', 'elysio-architect' ),
				'panel'	=> 'site_footer_panel', 
				'capability' => 'edit_theme_options',
			)
		);


		/**
		 * Site Footer Fonts Caption
		 */
		$wp_customize->add_setting( 'footer_fonts_caption',
			array(
				'default'		=> '#fff',
				'type'			=> 'theme_mod',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'refresh',
			)
		);
		$wp_customize->add_control(
			new Customizer_Library_Help_Text(
				$wp_customize,
				'footer_fonts_caption', 
				array(
					'label'			=> 'Site Footer Fonts',
					'section'		=> 'footer_style_section',
				)
			)
		);
		/**
		 * Site Footer Main Font CSS
		 */
		$wp_customize->add_setting( 'footer_font',
		   array(
		      'default' => '"Roboto Condensed", Sans-serif',
		      'transport' => 'refresh',
		      'sanitize_callback' => 'wp_strip_all_tags'
		   )
		);
		$wp_customize->add_control( 'footer_font',
		   array(
		      'label' => __( 'Main Font CSS', 'elysio-architect' ),
		      'description' => 'More about <a href="https://developer.mozilla.org/docs/Web/CSS/font" target="_blank">CSS font property</a>',
		      'section' => 'footer_style_section',
		      'type' => 'text',
		   )
		);
		/**
		 * Site Footer Headings Font CSS
		 */
		$wp_customize->add_setting( 'footer_headings_font',
		   array(
		      'default'	=> 'bold 1.75rem "Playfair Display", Serif',
		      'transport'	=> 'refresh',
		      'sanitize_callback' => 'wp_strip_all_tags'
		   )
		);
		$wp_customize->add_control( 'footer_headings_font',
		   array(
		      'label' => __( 'Headings Font CSS', 'elysio-architect' ),
		      'section' => 'footer_style_section',
		      'type' => 'text',
		   )
		);


		/**
		 * Site Footer Back To Top (Bump)
		 */
		$wp_customize->add_setting( 'footer_caption',
			array(
				'default'		=> '#fff',
				'type'			=> 'theme_mod',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'refresh',
			)
		);
		$wp_customize->add_control(
			new Customizer_Library_Help_Text(
				$wp_customize,
				'footer_caption', 
				array(
					'label'			=> __('Back-to-Top Button', 'elysio-architect'),
					'section'		=> 'footer_style_section',
					'description'	=> 'Allows users to smoothly scroll back to the top of the page.'
				)
			)
		);
		$wp_customize->add_setting( 'footer_bump_bgcolor',
			array(
				'default'    => '#fff',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio_footer_bump_bgcolor',
				array(
					'label'      => __( 'Arrow Color', 'elysio-architect' ),
					'settings'   => 'footer_bump_bgcolor',
					'section'    => 'footer_style_section',
				) 
		) );
		$wp_customize->add_setting( 'footer_bump_color',
			array(
				'default'    => '#cecece',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio_footer_bump_color',
				array(
					'label'      => __( 'Background Color', 'elysio-architect' ),
					'settings'   => 'footer_bump_color',
					'section'    => 'footer_style_section',
				) 
		) );

	}
}
add_action( 'customize_register', 'elysio_theme_customize_footer_styles' );


function footer_styles_customize_css()
{
?>
<style type="text/css">

	/* Elysio Site Footer Fonts Customizer */
	.site-footer { font: <?php echo get_theme_mod('footer_font', '"Roboto Condensed", Sans-serif'); ?>; }
	.site-footer .widget-title { font: <?php echo get_theme_mod('footer_headings_font', 'bold 1.75rem "Playfair Display", Serif'); ?>; }
	/* #END Elysio Site Footer Fonts Customizer */

	.site-footer .site-info {
		text-align: <?php echo get_theme_mod('footer_copyright_align', 'left'); ?>
	}

</style>
<?php
}
add_action( 'wp_head', 'footer_styles_customize_css');