<?php

/**
 * Elysio Theme Customizer - Header Mobile [TODO]
 *
 * @package elysio-architect
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


if ( ! function_exists( 'elysio_theme_customize_header_mobile' ) ) {
	function elysio_theme_customize_header_mobile( $wp_customize ) {

		/**
		 * Add Header Layout Section
		 */
		$wp_customize->add_section( 'header_mobile_section',
			array(
				'title' => __( 'Site Header Mobile', 'elysio-architect'  ),
			   	'description' => __( 'Site Header settings for Mobile.', 'elysio-architect' ),
			   	'panel' => 'site_header_panel',
			)
		);

		/**
		 * Site Header Logo Height on Mobile
		 */	
		$wp_customize->add_setting( 'site_logo_height_mobile',
		   array(
			  'default' => '',
			  'transport' => 'refresh',
		   )
		);
		$wp_customize->add_control( 'site_logo_height_mobile',
		   array(
				'label' => __( 'Logo Height (px)', 'elysio-architect' ),
				'section' => 'header_mobile_section',
				'type' => 'number', // Can be either text, email, url, number, hidden, or date
				'input_attrs' => array( // Optional.
					// 'class' => 'my-custom-class',
					// 'style' => 'border: 1px solid rebeccapurple',
					// 'placeholder' => __( 'Enter name...' ),
					'min' => 0,
				),
		   )
		);

		/**
		 * Site Header Search Icon on Mobile
		 */	
		$wp_customize->add_setting( 'header_show_search_mobile',
			array(
				'default' => 0,
				'transport' => 'refresh',
			)
		);
		$wp_customize->add_control( 'header_show_search_mobile',
			array(
				'label' => __( 'Show search icon', 'elysio-architect' ),
				'section'  => 'header_mobile_section',
				'priority' => 10,
				'type'=> 'checkbox',
			)
		);



	}
}
add_action( 'customize_register', 'elysio_theme_customize_header_mobile' );




function header_mobile_customize_css()
{
?>
<style type="text/css">
	@media (max-width: 767px){ 
		.site-header .custom-logo-link img {
			height: <?php echo get_theme_mod('site_logo_height_mobile', '36') . 'px'; ?>;
			width: auto;
		}
		<?php if ( ! get_theme_mod( 'header_show_search_mobile' ) ){
			echo '.header_search-icon { display: none; }';
		}
		?>
	}

</style>
<?php
}
add_action( 'wp_head', 'header_mobile_customize_css');


