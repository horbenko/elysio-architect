<?php

/**
 * Elysio Theme Customizer - Fonts
 *
 * @author artem.horbenko
 * @package elysio-architect
 */


// https://divpusher.com/blog/wordpress-customizer-sanitization-examples#text
// https://www.w3schools.com/cssref/pr_font_font.asp
// https://getbootstrap.com/docs/4.3/content/reboot/#native-font-stack


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_fonts' ) ) {
	function elysio_theme_customize_fonts( $wp_customize ) {
		
		/**
		 * Add Theme Fonts Section
		 */
		$wp_customize->add_section(
			'elysio_theme_fonts_options',
			array(
				'title'       => __( 'Fonts', 'elysio-architect' ),
				'capability'  => 'edit_theme_options',
			)
		);
		
		/**
		 * Main Text Font Size
		 */
		$wp_customize->add_setting( 'body_font_size',
			array(
				'default' => '16px',
				'transport' => 'refresh',
				'sanitize_callback' => 'wp_strip_all_tags'
			)
		);
		$wp_customize->add_control( 'body_font_size',
			array(
				'label' => __( 'Body Font Size', 'elysio-architect' ),
				'section' => 'elysio_theme_fonts_options',
				'type' => 'text', // Can be either text, email, url, number, hidden, or date
				'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			)
		);

		/**
		 * Main Text Font
		 */
		$wp_customize->add_setting( 'main_font',
			array(
				'default' => '"Roboto Condensed", Sans-serif',
				'transport' => 'refresh',
				'sanitize_callback' => 'wp_strip_all_tags'
			)
		);
		$wp_customize->add_control( 'main_font',
			array(
				'label' => __( 'Main Font', 'elysio-architect' ),
				'description' => 'More about <a href="https://developer.mozilla.org/docs/Web/CSS/font" target="_blank">CSS font property</a>',
				'section' => 'elysio_theme_fonts_options',
				'type' => 'text', // Can be either text, email, url, number, hidden, or date
				'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			)
		);

		/**
		 * Headings Font
		 */
		$wp_customize->add_setting( 'primary_font',
			 array(
				'default' => '"Playfair Display", Serif',
				'transport' => 'refresh',
				'sanitize_callback' => 'wp_strip_all_tags'
			 )
		);
		$wp_customize->add_control( 'primary_font',
			 array(
				'label' => __( 'Headings Font', 'elysio-architect' ),
				'section' => 'elysio_theme_fonts_options',
				'type' => 'text', // Can be either text, email, url, number, hidden, or date
			 )
		);

	}
}
add_action( 'customize_register', 'elysio_theme_customize_fonts' );


function fonts_customize_css()
{
?>
<style type="text/css">

	body 
	{ font-size: <?php echo get_theme_mod('body_font_size', '16px'); ?>; }
	body 
	{ font-family: <?php echo get_theme_mod('main_font', '"Roboto Condensed", Sans-serif'); ?>; }
	h1, h2, h3, h4, h5, h6 
	{ font-family: <?php echo get_theme_mod('primary_font', '"Playfair Display", Serif'); ?>; }


</style>
<?php
}
add_action( 'wp_head', 'fonts_customize_css');