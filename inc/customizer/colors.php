<?php

/**
 * Elysio Theme Customizer - Colors
 *
 * @author artem.horbenko
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_theme_customize_colors' ) ) {
	function elysio_theme_customize_colors( $wp_customize ) {

		/**
		 * Main Text Color
		 */
		$wp_customize->add_setting( 'main_text_color',
			array(
				'default'    => '#888888',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-text-color',
				array(
					'label'      => __( 'Main Text Color', 'elysio-architect' ),
					'settings'   => 'main_text_color',
					'section'    => 'colors',
				) 
		) );

		/**
		 * Primary Color
		 */
		$wp_customize->add_setting( 'link_color',
			array(
				'default'    => '#BD8881',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-link-color',
				array(
					'label'      => __( 'Link & Primary Color', 'elysio-architect' ),
					'settings'   => 'link_color',
					'section'    => 'colors',
				) 
		) );

		/**
		 * Primary Hover Color
		 */
		$wp_customize->add_setting( 'link_hover_color',
			array(
				'default'    => 'red',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-link-hover-color',
				array(
					'label'      => __( 'Link Hover Color', 'elysio-architect' ),
					'settings'   => 'link_hover_color',
					'section'    => 'colors',
				) 
		) );


		/**
		 * Buttons Colors Caption
		 */
		$wp_customize->add_setting( 'btn_caption',
			array(
				'default'    => '#fff',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control(
			new Customizer_Library_Help_Text(
				$wp_customize,
				'btn_caption', 
				array(
					'label'             => 'Button',
					'section'           => 'colors',

				)
			)
		);
		/**
		 * Button Color
		 */
		$wp_customize->add_setting( 'btn_color',
			array(
				'default'    => '#ffffff',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-btn-color',
				array(
					'label'      => __( 'Button Color', 'elysio-architect' ),
					'settings'   => 'btn_color',
					'section'    => 'colors',
				) 
		) );
		/**
		 * Button Background Color
		 */
		$wp_customize->add_setting( 'btn_bg_color',
			array(
				'default'    => '#BD8881',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-btn-bg-color',
				array(
					'label'      => __( 'Button Background', 'elysio-architect' ),
					'settings'   => 'btn_bg_color',
					'section'    => 'colors',
				) 
		) );
		/**
		 * Button Hover Color
		 */
		$wp_customize->add_setting( 'btn_hover_color',
			array(
				'default'    => '#ffffff',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-btn-hover-color',
				array(
					'label'      => __( 'Button Hover Color', 'elysio-architect' ),
					'settings'   => 'btn_hover_color',
					'section'    => 'colors',
				) 
		) );
		/**
		 * Button Hover Background Color
		 */
		$wp_customize->add_setting( 'btn_hover_bg_color',
			array(
				'default'    => '#BD8881',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'   => 'refresh',
			)
		);
		$wp_customize->add_control( new WP_Customize_Color_Control(
			$wp_customize,
			'elysio-btn-hover-bg-color',
				array(
					'label'      => __( 'Button Hover Background', 'elysio-architect' ),
					'settings'   => 'btn_hover_bg_color',
					'section'    => 'colors',
				) 
		) );

	}

}
add_action( 'customize_register', 'elysio_theme_customize_colors' );




function colors_customize_css()
{
?>
<style type="text/css">

	/* Background color */
	.custom-background { background-color: #<?php echo get_theme_mod('background_color', 'ffffff'); ?>; }
	/* #END Background color */

	/* Main text color */
	body,
	input,
	textarea
	{
		color: <?php echo esc_html(get_theme_mod('main_text_color', '#888888')); ?>;
	}
	.elysio-post-navigation a
	{
		color: <?php echo esc_html(get_theme_mod('main_text_color', '#888888')); ?>;
	}
	/* #END Main text color */


	/* Primary color */
	a { color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>; }
	a:hover { color: <?php echo get_theme_mod('link_hover_color', 'red'); ?>; }
	.header_search-close_modal svg path:first-child {
		fill: <?php echo get_theme_mod('link_color', 'red'); ?>;
	}

	.elysio-post-navigation a:hover {
		color: <?php echo esc_html(get_theme_mod('link_color', '#BD8881')); ?>;
	}
	.elysio-post-navigation a:hover svg {
		fill: <?php echo esc_html(get_theme_mod('link_color', '#BD8881')); ?>;
	}

	.bar:before { 
		background: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}
	input:focus~label, input:valid~label, textarea:focus~label, textarea:valid~label {
		color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}

	.page-item.active .page-link {
		background-color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
		border-color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}
	.page-link {
		color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}
	.page-link svg {
		fill: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}
	.page-link:hover {
		color: <?php echo get_theme_mod('link_hover_color', 'red'); ?>;
	}
	/* #END Primary color */


	/* Colors Comments Customizer */
	.comment-form .btn-primary {
		color: #<?php echo get_theme_mod('background_color', 'ffffff'); ?>;
		background-color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
		border-color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}
	.comment-form .btn-primary:hover {
		color: #<?php echo get_theme_mod('background_color', 'ffffff'); ?>;
		background-color: <?php echo get_theme_mod('link_hover_color', '#BD8881'); ?>;
		border-color: <?php echo get_theme_mod('link_hover_color', '#BD8881'); ?>;
	}

	.elysio-comments-area-01 .form-control {
		border-color: <?php echo esc_html(get_theme_mod('main_text_color', '#888888')); ?>;
	}
	.elysio-comments-area-01 .form-control:active,
	.elysio-comments-area-01 .form-control:focus
	{
			border-color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}
	/* #END Colors Comments Customizer */




	/* Colors: Forms, Buttons, Textfiels, ContactForm7 - Customizer */

	.form-control, .wpcf7 .wpcf7-validation-errors, .wpcf7 input[type=color], .wpcf7 input[type=date], .wpcf7 input[type=datetime-local], .wpcf7 input[type=datetime], .wpcf7 input[type=email], .wpcf7 input[type=file], .wpcf7 input[type=month], .wpcf7 input[type=number], .wpcf7 input[type=range], .wpcf7 input[type=search], .wpcf7 input[type=submit], .wpcf7 input[type=tel], .wpcf7 input[type=text], .wpcf7 input[type=time], .wpcf7 input[type=url], .wpcf7 input[type=week], .wpcf7 select, .wpcf7 textarea
	{
		color: <?php echo esc_html(get_theme_mod('main_text_color', '#888888')); ?>;
	}

	.form-control:focus, .wpcf7 .wpcf7-validation-errors:focus, .wpcf7 input:focus[type=color], .wpcf7 input:focus[type=date], .wpcf7 input:focus[type=datetime-local], .wpcf7 input:focus[type=datetime], .wpcf7 input:focus[type=email], .wpcf7 input:focus[type=file], .wpcf7 input:focus[type=month], .wpcf7 input:focus[type=number], .wpcf7 input:focus[type=range], .wpcf7 input:focus[type=search], .wpcf7 input:focus[type=submit], .wpcf7 input:focus[type=tel], .wpcf7 input:focus[type=text], .wpcf7 input:focus[type=time], .wpcf7 input:focus[type=url], .wpcf7 input:focus[type=week], .wpcf7 select:focus, .wpcf7 textarea:focus
	{
		border-color: <?php echo get_theme_mod('link_color', '#BD8881'); ?>;
	}

	.btn-outline-primary, .wpcf7 input[type=submit] {
		background-color: <?php echo get_theme_mod('btn_bg_color', '#BD8881'); ?>;
		border-color: <?php echo get_theme_mod('btn_bg_color', '#BD8881'); ?>;
		color: <?php echo get_theme_mod('btn_color', '#ffffff'); ?>;
	}
	.btn-outline-primary:hover, .wpcf7 input[type=submit]:hover {
		background-color: <?php echo get_theme_mod('btn_hover_bg_color', '#ffffff'); ?> !important;
		border-color: <?php echo get_theme_mod('btn_hover_bg_color', '#ffffff'); ?> !important;
		color: <?php echo get_theme_mod('btn_hover_color', '#BD8881'); ?> !important;
	}

	/* #END Colors: Forms, Buttons, Textfiels, ContactForm7 - Customizer */


	/* Colors Site Header Search Customizer */
	.header_search { background-color: <?php echo get_theme_mod('background_color', '#ffffff'); ?> !important; }
	.header_search { color: <?php echo get_theme_mod('main_text_color', '#888888'); ?> !important; }
	/* #END Colors Site Header Search Customizer */

	/* Colors Site Header Searchform Customizer */
	form.searchform input
	{
		color: <?php echo get_theme_mod('main_text_color', '#888888'); ?>;
		border-bottom: 2px solid <?php echo get_theme_mod('main_text_color', '#888888'); ?>;
	}
	form.material-form .bar:before {
		background: <?php echo get_theme_mod('link_color', '#bd8881'); ?>;
	}
	form.material-form input:focus~label, 
	form.material-form input:valid~label, 
	form.material-form textarea:focus~label, 
	form.material-form textarea:valid~label
	{
		color: <?php echo get_theme_mod('link_color', '#bd8881'); ?>;
	}
	/* #END Colors Site Header Searchform Customizer */

</style>
<?php
}
add_action( 'wp_head', 'colors_customize_css');