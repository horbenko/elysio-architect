<?php

/**
 * Elysio Theme Customizer - Header Layout [TODO]
 *

  - Header Layout: Classic/transparent
  + Header Fixed
  + Menu Position: Left/Center/Right
  + Search Icon

 * @package elysio-architect
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


if ( ! function_exists( 'elysio_theme_customize_header_layout' ) ) {
	function elysio_theme_customize_header_layout( $wp_customize ) {
		
		
	/**
	 * Add Header Layout Section
	 */
	$wp_customize->add_section( 'header_layout_section',
		array(
			'title' => __( 'Header Layout', 'elysio-architect'  ),
			// 'description' => esc_html__( 'Site header of Elysio theme.' ),
			'panel' => 'site_header_panel', // Only needed if adding your Section to a Panel
			'capability' => 'edit_theme_options', // Not typically needed. Default is edit_theme_options
			'theme_supports' => '', // Rarely needed
			'active_callback' => '', // Rarely needed
			'description_hidden' => 'false', // Rarely needed. Default is False
		)
	);

		/**
		 * Site Header Height
		 */
		// $wp_customize->add_setting( 'site_header_height',
		//    array(
		// 	  'default' => '96',
		// 	  'transport' => 'refresh',
		//    )
		// );
		// $wp_customize->add_control( 'site_header_height',
		//    array(
		// 	  'label' => __( 'Header Height (px)', 'elysio-architect' ),
		// 	  'section' => 'header_layout_section',
		// 	  'type' => 'number', // Can be either text, email, url, number, hidden, or date
		// 	  'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
		// 	  'input_attrs' => array(
		// 		 'min' => 0,
		// 	  ),
		//    )
		// );

		/**
		 * Site Header Mobile Height
		 */
		// $wp_customize->add_setting( 'site_header_mobile_height',
		//    array(
		// 	  'default' => '64',
		// 	  'transport' => 'refresh',
		//    )
		// );
		// $wp_customize->add_control( 'site_header_mobile_height',
		//    array(
		// 	  'label' => __( 'Header Mobile Height (px)', 'elysio-architect' ),
		// 	  'section' => 'header_layout_section',
		// 	  'type' => 'number', // Can be either text, email, url, number, hidden, or date
		// 	  'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
		// 	  'input_attrs' => array(
		// 		 'min' => 0,
		// 	  ),
		//    )
		// );


		/**
		 * Site Header Logo Height
		 */	
		$wp_customize->add_setting( 'site_logo_height',
		   array(
			  'default' => '',
			  'transport' => 'refresh',
			  // 'sanitize_callback' => 'skyrocket_text_sanitization'
		   )
		);
		$wp_customize->add_control( 'site_logo_height',
		   array(
			  'label' => __( 'Logo Height (px)', 'elysio-architect' ),
			  // 'description' => esc_html__( 'Text controls Type can be either text, email, url, number, hidden, or date' ),
			  // 'section' => 'title_tagline',
			  'section' => 'header_layout_section',
			  'priority' => 10, // Optional. Order priority to load the control. Default: 10
			  'type' => 'number', // Can be either text, email, url, number, hidden, or date
			  'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			  'input_attrs' => array( // Optional.
				 // 'class' => 'my-custom-class',
				 // 'style' => 'border: 1px solid rebeccapurple',
				 // 'placeholder' => __( 'Enter name...' ),
				 'min' => 0,
			  ),
		   )
		);




		 // Header Settings

		// Layout
		 // $wp_customize->add_setting( 'elysio_header_layout',
		 //    array(
		 //       'default' => 'jet-fuel',
		 //       'transport' => 'refresh',
		 //       // 'sanitize_callback' => 'skyrocket_radio_sanitization'
		 //    )
		 // );
		 // $wp_customize->add_control( 'elysio_header_layout',
		 //    array(
		 //       'label' => __( 'Header Layout' ),
		 //       // 'description' => esc_html__( 'Sample description' ),
		 //       'section' => 'header_layout_section',
		 //       'priority' => 10, // Optional. Order priority to load the control. Default: 10
		 //       'type' => 'select',
		 //       'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
		 //       'default' => 'classic', // Optional. Default: 'edit_theme_options'
		 //       'choices' => array( // Optional.
		 //          'classic' => __( 'Classic' ),
		 //          'transparent' => __( 'Transparent' ),
		 //          // 'nuclear-energy' => __( 'Nuclear Energy' )
		 //       )
		 //    )
		 // );

		 // Fixed 
		 // $wp_customize->add_setting( 'header_fixed_checkbox',
			// array(
			//    'default' => 0,
			//    'transport' => 'refresh',
			//    // 'sanitize_callback' => 'skyrocket_switch_sanitization'
			// )
		 // );
		 // $wp_customize->add_control( 'header_fixed_checkbox',
			// array(
			//    'label' => __( 'Fixed Header', 'elysio-architect' ),
			//    // 'description' => esc_html__( 'Sample description' ),
			//    'section'  => 'header_layout_section',
			//    'priority' => 10, // Optional. Order priority to load the control. Default: 10
			//    'type'=> 'checkbox',
			//    'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			// )
		 // );




		 /* Site Header Menu Layout */
		 // $wp_customize->add_setting( 'site_header_menu_layout',
		 //    array(
		 //       'default' => 'jet-fuel',
		 //       'transport' => 'refresh',
		 //    )
		 // );
		 // $wp_customize->add_control( 'site_header_menu_layout',
		 //    array(
		 //       'label' => __( 'Menu Layout' ),
		 //       'section' => 'header_layout_section',
		 //       'type' => 'select',
		 //       'default' => 'default', 
		 //       'choices' => array(
		 //          'default' => __( 'Default' ),
		 //          'fullscreen' => __( 'Fullscreen' ),
		 //       )
		 //    )
		 // );


		// Menu Position
		 $wp_customize->add_setting( 'elysio_header_menu_position',
			array(
			   'default' => 'jet-fuel',
			   'transport' => 'refresh',
			   // 'sanitize_callback' => 'skyrocket_radio_sanitization'
			)
		 );
		 $wp_customize->add_control( 'elysio_header_menu_position',
			array(
			   'label' => __( 'Menu Position', 'elysio-architect' ),
			   // 'description' => esc_html__( 'Sample description' ),
			   'section' => 'header_layout_section',
			   'priority' => 10, // Optional. Order priority to load the control. Default: 10
			   'type' => 'select',
			   'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			   'default' => 'right', 
			   'choices' => array(
				  'left' => __( 'Left', 'elysio-architect' ),
				  'center' => __( 'Center', 'elysio-architect' ),
				  'justify' => __( 'Justify', 'elysio-architect' ),
				  'right' => __( 'Right', 'elysio-architect' ),
			   )
			)
		 );

		 // Search Icon
		 $wp_customize->add_setting( 'header_show_search',
			array(
			   'default' => 0,
			   'transport' => 'refresh',
			)
		 );
		 $wp_customize->add_control( 'header_show_search',
			array(
			   'label' => __( 'Show search icon', 'elysio-architect' ),
			   'section'  => 'header_layout_section',
			   'priority' => 10,
			   'type'=> 'checkbox',
			   'capability' => 'edit_theme_options',
			)
		 );





		$wp_customize->add_setting( 'header_shadow',
		   array(
			  'default' => 0,
			  'transport' => 'refresh',
		   )
		);
		$wp_customize->add_control( 'header_shadow',
		   array(
			  'label' => __( 'Header Shadow', 'elysio-architect' ),
			  // 'description' => esc_html__( 'Sample description' ),
			  'section'  => 'header_layout_section',
			  'type'=> 'checkbox',
		   )
		);

		$wp_customize->add_setting( 'header_rounded',
		   array(
			  'default' => 0,
			  'transport' => 'refresh',
		   )
		);
		$wp_customize->add_control( 'header_rounded',
		   array(
			  'label' => __( 'Header Rounded', 'elysio-architect' ),
			  // 'description' => esc_html__( 'Sample description' ),
			  'section'  => 'header_layout_section',
			  'type'=> 'checkbox',
		   )
		);





	}
}


add_action( 'customize_register', 'elysio_theme_customize_header_layout' );

function header_layout_customize_css()
{
  ?>
  <style type="text/css">
	  
	  
	/* Site Header Height */
	@media (min-width: 768px){

		<?php if ( ! get_theme_mod( 'header_show_search' ) ){
			echo '.header_search-icon { display: none; }';
		}
		?>
		
	  /*.navbar>.container, .navbar>.container-fluid {
		min-height: <?php echo get_theme_mod('site_header_height', '96') . 'px'; ?>;
	  }*/
	  /*.navbar-fullscreen .navbar-nav {
		top: <?php echo get_theme_mod('site_header_height', '96') . 'px'; ?>;
		height: calc(100vh - <?php echo get_theme_mod('site_header_height', '64') . 'px'; ?>);
	  }*/
	  /*.main-navigation .navbar-nav>.menu-item {
		min-height: <?php echo get_theme_mod('site_header_height', '96') . 'px'; ?>;
	  }*/

	}
	/* Site Mobile Header Height */
	@media (max-width: 767px){
	  /*.navbar>.container, .navbar>.container-fluid {
		min-height: <?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?>;
	  }*/
	 /* .navbar-fullscreen .navbar-nav {
		top: <?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?>;
		height: calc(100vh - <?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?>);
	  }*/
	}
	  
	  
	  
	  
	.site-header .custom-logo-link img {
	  height: <?php echo get_theme_mod('site_logo_height', '36') . 'px'; ?>;
	  width: auto;
	}
	  
	  
	  
	/*  Header Layout  */
	<?php
	  //if( get_theme_mod( 'header_fixed_checkbox' ) ){

			/*body {
			  padding-top: <?php echo get_theme_mod('site_header_height', '64') . 'px'; ?>;
			}
			body.page-template-default, body.post-template-default, body.page-template-left-sidebarpage, body.page-template-right-sidebarpage
				{
				padding-top: <?php echo get_theme_mod('site_header_height', '64') . 'px'; ?>;
				padding-top: calc(<?php echo get_theme_mod('site_header_height', '64') . 'px'; ?> + 2em);
			}
			body.elementor-page {
			  padding-top: <?php echo get_theme_mod('site_header_height', '64') . 'px'; ?>;
			}
			@media (max-width: 767px){
			
				body {
				  padding-top: <?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?>;
				}
				body.page-template-default, body.post-template-default, body.page-template-left-sidebarpage, body.page-template-right-sidebarpage
					{
					padding-top: <?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?>;
					padding-top: calc(<?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?> + 2em);
				}
				body.elementor-page {
				  padding-top: <?php echo get_theme_mod('site_header_mobile_height', '64') . 'px'; ?>;
				}

			}*/

	  //}
	?>

  </style>
  <?php
}
add_action( 'wp_head', 'header_layout_customize_css');



if( get_theme_mod('header_show_search') ){
  // add the ajax fetch js
  add_action( 'wp_footer', 'ajax_fetch' );
  function ajax_fetch() {
  ?>
  <script type="text/javascript">

  document.querySelector('#headerSearch input').onkeyup = fetch;


  function fetch(){

	  jQuery.ajax({
		  url: '<?php echo admin_url('admin-ajax.php'); ?>',
		  type: 'post',
		  data: { action: 'data_fetch', keyword: jQuery('#headerSearch input').val() },
		  success: function(data) {
			  jQuery('#headerSearchResult').html( data );
		  }
	  });

  }
  </script>

  <?php
  }
}