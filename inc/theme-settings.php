<?php
/**
 * Check and setup theme's default settings
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elysio_setup_theme_default_settings' ) ) {
	function elysio_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$elysio_posts_index_style = get_theme_mod( 'elysio_posts_index_style' );
		if ( '' == $elysio_posts_index_style ) {
			set_theme_mod( 'elysio_posts_index_style', 'default' );
		}

		// Sidebar position.
		$elysio_sidebar_position = get_theme_mod( 'elysio_sidebar_position' );
		if ( '' == $elysio_sidebar_position ) {
			set_theme_mod( 'elysio_sidebar_position', 'right' );
		}

		// Container width.
		$elysio_container_type = get_theme_mod( 'elysio_container_type' );
		if ( '' == $elysio_container_type ) {
			set_theme_mod( 'elysio_container_type', 'container' );
		}

		// Copyright.
		$elysio_copyright = get_theme_mod( 'footer_copyright' );
		if ( '' == $elysio_copyright ) {
			set_theme_mod( 'footer_copyright', '© Copyright 2020 Elysio Architect' );
		}
	}
}
