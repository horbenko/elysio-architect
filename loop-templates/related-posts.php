<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
?>

<article <?php post_class('col-md-4'); ?> id="post-<?php the_ID(); ?>">

  <a href="<?php echo get_permalink(); ?>" class="entry-thumb">
    <?php
      echo get_the_post_thumbnail( $post->ID, 'a4by3' ); 
    ?>
  </a>

  <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
    '</a></h2>' ); ?>


  <div class="entry-content">

    <?php
    the_excerpt();
    ?>

  </div><!-- .entry-content -->

  <?php 
  if( ! get_theme_mod( 'archives_setting_more', __( '', 'elysio-architect' ) ) ){
  ?>
    <p class="entry-footer">
      <a class="understrap-read-more-link" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>">
       <?php echo __( 'Read More...', 'elysio-architect' )  ?>
      </a>
    </p>
  <?php
  }
  ?>

</article><!-- #post-## -->
