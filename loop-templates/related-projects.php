<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package elysio-architect
 */
 
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
?>

<article <?php post_class('col-12 col-sm-12 col-md-4'); ?> id="post-<?php the_ID(); ?>">

  <div class="entry-content">
    <div class="entry-thumb">
      <?php
        echo get_the_post_thumbnail( $post->ID, 'a1by1' ); 
      ?>
    </div>
    <div class="entry-thumb__overlay"></div>

    <div class="entry-copy">

      <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

      <p class="entry-footer"><a class="understrap-read-more-link" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>"><?php echo __( 'Watch More...', 'elysio-architect' )  ?></a></p>

    </div><!-- .entry-content -->
  </div>

</article><!-- #post-## -->
