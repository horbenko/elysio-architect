

document.addEventListener('DOMContentLoaded', function(){ 
    // alert('Hello world');
    // console.log('Hi')

    // Header Search + Modal Search
    var element__hssm =  document.getElementById('hssm');
    var element__hscm =  document.getElementById('hscm');
    if (typeof(element__hssm) != 'undefined' && element__hssm != null)
    {
      hssm.onclick = function(){
        // headerSearch.style.display = 'flex';
        jQuery('#headerSearch').fadeIn('fast');
        setTimeout(function(){
          jQuery('#headerSearch #s').focus();
        }, 600);

        var bodyRect = document.body.getBoundingClientRect(),
            elemRect = element__hssm.getBoundingClientRect();

        element__hscm.style.top = elemRect.top + 'px';
        element__hscm.style.left = elemRect.left + 'px';
      }
      hscm.onclick = function(){
        // headerSearch.style.display = 'none';
        jQuery('#headerSearch').fadeOut('fast');
      }
      document.onkeydown = function(evt) {
          evt = evt || window.event;
          var isEscape = false;
          if ("key" in evt) {
              isEscape = (evt.key === "Escape" || evt.key === "Esc");
          } else {
              isEscape = (evt.keyCode === 27);
          }
          if (isEscape) {
              // alert("Escape");
          headerSearch.style.display = 'none';
          }
      };
    }



    // Bump
    var element__bump =  document.getElementById('bump');
    if (typeof(element__bump) != 'undefined' && element__bump != null)
    {
        window.addEventListener('scroll', function(){
          if( window.scrollY > window.innerHeight ){
              jQuery('#bump').fadeIn('fast');
          }else{
              jQuery('#bump').fadeOut('fast');
          }
        })
      bump.onclick = function(){
          window.scrollTo(0,0);
      }
    }
    


    // Transparent Header | Window ScrollTop almost 0
    var elementHeader =  document.getElementById('site-header');
    if (typeof(elementHeader) != 'undefined' && elementHeader != null)
    {
        if( window.scrollY > document.querySelector('#site-header .elysio-navbar').clientHeight ){
            document.querySelector('#site-header .elysio-navbar').classList.remove('windowScrollY0');
        }else{
            document.querySelector('#site-header .elysio-navbar').classList.add('windowScrollY0');
        }
      // exists.
        window.addEventListener('scroll', function(){
            if( window.scrollY > document.querySelector('#site-header .elysio-navbar').clientHeight ){
                document.querySelector('#site-header .elysio-navbar').classList.remove('windowScrollY0');
            }else{
                document.querySelector('#site-header .elysio-navbar').classList.add('windowScrollY0');
            }
        })
    }
}, false);





jQuery(function ($) {
  elysioGoogleMapInitialize = function() {
    jQuery( '.elysio-map' ).each( function( index, element ) {

      var $this = jQuery(element);

      var uluru = { lat: parseFloat($this.data( 'map-latitude' )), lng: parseFloat($this.data( 'map-longitude' )) };

      var map = new google.maps.Map( document.getElementById($this.attr('id')), {
        zoom: $this.data( 'map-zoom' ),
        center: uluru,
        //disableDefaultUI: true,
        scrollwheel: $this.data( 'map-scroll' ),
        styles: $this.data( 'map-style' ),
      } );

      var marker = new google.maps.Marker({position: uluru, map: map});

    } );

  };

  elysioSetupGoogleMaps = function() {

    var mapAPI = gmapi;

    var mapsApiLoaded = typeof window.google !== 'undefined' && typeof window.google.maps !== 'undefined';
    if ( mapsApiLoaded ) {
      elysioGoogleMapInitialize();
    } else {
      var apiUrl = 'https://maps.googleapis.com/maps/api/js?callback=elysioGoogleMapInitialize';

      if ( mapAPI ) {
        apiUrl += '&key=' + mapAPI;
      }

      $( 'body' ).append( '<script async type="text/javascript" src="' + apiUrl + '">' );
    }
  };


});



