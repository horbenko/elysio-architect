<?php
/**
 * Understrap functions and definitions
 *
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/elysio-tags.php',
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	
	'/customizer.php',                      // Customizer additions.
	'/customizer/customizer.php',
	'/customizer/Customizer_Library_Help_Text.php',
	'/customizer/theme.php',
	'/customizer/layout.php',
	'/customizer/colors.php',
	'/customizer/fonts.php',
	'/customizer/header.php',
	'/customizer/header-layout.php',
	'/customizer/header-mobile.php',
	'/customizer/footer.php',
	'/customizer/footer-styles.php',
	'/customizer/blog.php',
	'/search.php',							// AJAX Search for site header search popup.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.

);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}



// Required plugins
require_once get_theme_file_path( 'inc/libs/register-plugins.php' );
require_once get_theme_file_path( 'inc/libs/class-tgm-plugin-activation.php' );
add_action( 'tgmpa_register', 'ah_register_required_plugins' );



// Merlin
require_once get_parent_theme_file_path( '/inc/libs/merlin/vendor/autoload.php' );
require_once get_parent_theme_file_path( '/inc/libs/merlin/class-merlin.php' );
require_once get_parent_theme_file_path( '/inc/libs/merlin-config.php' );

// Kirki
include_once get_theme_file_path( '/inc/libs/class-kirki-installer-section.php' );

Kirki::add_config( 'elysio_config', array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

include_once get_theme_file_path( '/inc/customizer/contacts-info.php' );
include_once get_theme_file_path( '/inc/customizer/site-footer.php' );

