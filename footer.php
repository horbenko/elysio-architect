<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content div and all content after
 *
 * @author artem.horbenko
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'elysio_container_type' );
?>

	<footer id="site-footer" class="site-footer" role="contentinfo">

		<div class="<?php echo esc_attr( $container ); ?>" id="footer-full-content" tabindex="-1">

			<?php if ( is_active_sidebar( 'footerfull' ) ) : ?>
				<div class="row">

					<?php dynamic_sidebar( 'footerfull' ); ?>

				</div>
			<?php endif; ?>


		</div>

		<div class="site-info">
			<div class="<?php echo esc_attr( $container ); ?>">
				<div class="row align-items-center">
					<div class="col-12 col-md-6">
						<div class="copyright">
							<?php 
							echo esc_html( get_theme_mod( 'footer_copyright' ) );
							?>
						</div>
					</div>
					<div class="col-12 col-md-6">
						<?php elysio_contacts_bar(); ?>
					</div>
			</div>
		</div><!-- .site-info -->

		<!-- BUMP -->
		<?php
		if( get_theme_mod( 'footer_bump_checkbox' ) ){
		?>
			<div class="bump" id="bump"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path class="bump-icon-path" d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"/><path d="M0 0h24v24H0z" fill="none"/></svg></div>
		<?php
		}
		?>

	</footer>


</div><!-- #page we need this extra closing tag here -->

<script type="text/javascript">
	window.gmapi = '<?php echo get_theme_mod( 'google_maps_api' ); ?>';
</script>
<?php wp_footer(); ?>

</body>
</html>